import pandas as pd
import numpy as np
import matplotlib
import matplotlib.dates as md

matplotlib.use('Qt4Agg')

import matplotlib.pyplot as plt

df = pd.read_csv('data/ninemsn-1.tsv', sep="\t")

df.plot(x='seconds', y='ttime', kind='scatter', label='response times'); plt.legend(loc='best')

ax=plt.gca()
#xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
#ax.xaxis.set_major_formatter(xfmt)

plt.show()
